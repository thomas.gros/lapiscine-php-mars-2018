<?php

// POST = le formulaire a été soumis par l'utilisateur
if("POST" == $_SERVER["REQUEST_METHOD"]) {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine', 'root', 'root');

        $stmt = $dbh->prepare('INSERT INTO article (title, content) VALUES (:title, :content)');
        $stmt->bindParam(':title', $_POST['title']);
        $stmt->bindParam(':content', $_POST['content']);
        $stmt->execute();

        $dbh = null;
} else {
// afficher le formulaire si GET ou si erreurs détectées
    include 'form-article-view.php';
}

