<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<form action="" method="post">
    <div>
        <label for="title">titre</label>
        <input id="title" name="title" type="text">
    </div>

    <div>
        <label for="content">contenu</label>
        <textarea id="content" name="content" cols="30" rows="10"></textarea>
    </div>

    <div>
        <input type="submit" value="créer un article">
    </div>
</form>

</body>
</html>