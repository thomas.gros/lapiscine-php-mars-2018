<?php

$dbh = new PDO('mysql:host=localhost;dbname=lapiscine', 'root', 'root');

$id = $_GET['id'];

$stmt = $dbh->prepare('SELECT * from article where id = :id');
$stmt->bindParam(':id', $id);
$stmt->execute();
$result = $stmt->fetchAll();

if(count($result) == 0) {
    http_response_code(404);
    echo 'page not found';
} else {
    // var_dump($result);

    $articles = [];

    foreach ($result as $row) {
        $articles[]= $row;
    }

    $dbh = null;

    include 'detail-view.php';
}