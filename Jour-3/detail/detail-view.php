<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

<section class="articles">

    <?php foreach($articles as $article) : ?>
        <article class="article">
            <header>
                <h3><?= $article['title'] ?></h3>
                <p><?= $article['created_at'] ?></p>
            </header>
            <p><?= $article['content'] ?></p>
        </article>
    <?php endforeach ?>

</section>

</body>
</html>