<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

<section class="articles">

    <?php foreach($articles as $article) : ?>
        <article class="article">
            <header>

                <a href="http://localhost:8888/LaPiscine/Jour-3/detail/detail-code.php?id=<?= $article['id'] ?>">
                    <h3>
                        <?= $article['title'] ?>
                    </h3>
                </a>

                <p><?= $article['created_at'] ?></p>
            </header>
        </article>
    <?php endforeach ?>

</section>

</body>
</html>