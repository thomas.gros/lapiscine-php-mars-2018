<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

<section class="articles">

    <?php foreach($articles as $article) : ?>
        <article class="article">
            <header>

                <a href="http://localhost:8888/LaPiscine/Jour-8/Controller/article-detail-code.php?id=<?= $article->getId() ?>">
                    <h3>
                        <?= $article->getTitle() ?>
                    </h3>
                </a>

                <p><?= $article->getDescription() ?></p>
            </header>
        </article>
    <?php endforeach ?>

</section>

</body>
</html>