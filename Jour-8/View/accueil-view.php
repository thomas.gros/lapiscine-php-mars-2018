<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>
<h1>Accueil</h1>

<nav>
    <ul>
        <li><a href="">Accueil</a></li>
        <li><a href="http://localhost:8888/LaPiscine/Jour-8/Controller/article-master-code.php">Articles</a></li>
        <li><a href="http://localhost:8888/LaPiscine/Jour-8/Controller/client-master-code.php">Clients</a></li>
    </ul>
</nav>
<section class="articles">

    <table>
        <thead>
        <tr><th>3 derniers articles</th></tr>
        </thead>
        <tbody>
        <?php foreach($articles as $article) : ?>
            <tr>
                <td>
                    <a href="http://localhost:8888/LaPiscine/Jour-8/Controller/article-detail-code.php?id=<?= $article->getId() ?>">
                        <?= $article->getTitle() ?>
                    </a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

    <a href="http://localhost:8888/LaPiscine/Jour-8/Controller/article-master-code.php">tous les articles</a>
</section>


<section class="client">

    <table>
        <thead>
            <tr><th>3 derniers clients</th></tr>
        </thead>
        <tbody>
        <?php foreach($clients as $client) : ?>
            <tr>
                <td>
                    <a href="http://localhost:8888/LaPiscine/Jour-8/Controller/client-detail-code.php?id=<?= $client->getId() ?>">
                        <?= $client->getName() ?>
                    </a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
    <a href="http://localhost:8888/LaPiscine/Jour-8/Controller/client-master-code.php">tous les clients</a>
</section>

</body>
</html>