<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

    <article class="client">
        <header>
            <h3><?= $client->getId() ?></h3>
        </header>
        <p><?= $client->getName() ?></p>
    </article>

</body>
</html>