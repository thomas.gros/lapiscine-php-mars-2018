<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

    <article class="article">
        <header>
            <h3><?= $article->getTitle() ?></h3>
        </header>
        <p><?= $article->getDescription() ?></p>
    </article>

</body>
</html>