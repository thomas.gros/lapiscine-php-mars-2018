<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

<section class="clients">

    <?php foreach($clients as $client) : ?>
        <article class="client">
            <header>

                <a href="http://localhost:8888/LaPiscine/Jour-8/Controller/client-detail-code.php?id=<?= $client->getId() ?>">
                    <h3>
                        <?= $client->getName() ?>
                    </h3>
                </a>
            </header>
        </article>
    <?php endforeach ?>

</section>

</body>
</html>