<?php

require '../Model/ArticleRepository.php';

$articleRepository = new ArticleRepository();
$articles = $articleRepository->findAllArticles();

include '../View/article-master-view.php';