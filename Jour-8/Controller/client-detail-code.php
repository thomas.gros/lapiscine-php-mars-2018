<?php

require '../Model/ClientRepository.php';

$id = $_GET['id'];

$clientRepository = new ClientRepository();
$client = $clientRepository->findById($id);

if($client === null) {
    http_response_code(404);
    echo 'page not found';
} else {
    include '../View/client-detail-view.php';
}