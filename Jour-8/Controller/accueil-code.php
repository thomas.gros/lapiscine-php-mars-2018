<?php

require '../Model/ArticleRepository.php';
require '../Model/ClientRepository.php';

$articleRepository = new ArticleRepository();
$clientRepository = new ClientRepository();

$articles = $articleRepository->findLatest3Articles();
$clients = $clientRepository->findLatest3Clients();

include '../View/accueil-view.php';