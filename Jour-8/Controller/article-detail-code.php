<?php

require '../Model/ArticleRepository.php';

$id = $_GET['id'];

$articleRepository = new ArticleRepository();
$article = $articleRepository->findById($id);

if($article === null) {
    http_response_code(404);
    echo 'page not found';
} else {
    include '../View/article-detail-view.php';
}