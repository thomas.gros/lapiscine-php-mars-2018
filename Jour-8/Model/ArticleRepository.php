<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 13/03/2018
 * Time: 15:54
 */

require 'Article.php';

/**
 * Pour plus d'info sur les repository, lire Domain Driven Design
 * Class ArticleRepository
 */
class ArticleRepository
{

    /**
     * Retourne tous les articles.
     * @return array d'objet de type Article. Si aucun article n'est trouvé, retourne un tableau vide.
     */
    function findAllArticles()
    {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine_commerce', 'root', 'root');

        $result = $dbh->query('SELECT * from article');

        $articles = [];

        foreach($result as $row) {
            $article = new Article($row['id'], $row['title'], $row['description'], $row['price']);
            $articles []= $article;
        }

        $dbh = null;
        return $articles;
    }

    /**
     * Retourne les 3 derniers articles
     * @return array d'objet de type Article. Si aucun article n'est trouvé, retourne un tableau vide.
     */
    function findLatest3Articles()
    {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine_commerce', 'root', 'root');

        $result = $dbh->query('SELECT * from article ORDER BY id DESC LIMIT 3');

        $articles = [];

        foreach($result as $row) {
            $article = new Article($row['id'], $row['title'], $row['description'], $row['price']);
            $articles []= $article;
        }

        $dbh = null;
        return $articles;
    }



    /**
     * Retourne l'article dont l'identifiant est précisé en paramètre.
     * @param $id identifiant de l'article à retourner
     * @return Article si l'identifiant existe. Retourne null sinon.
     */
    public function findById($id)
    {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine_commerce', 'root', 'root');

        $stmt = $dbh->prepare('SELECT * from article where id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $result = $stmt->fetch();

        if($result === false) {
            return null;
        }

        $article = new Article($result['id'], $result['title'], $result['description'], $result['price']);

        $dbh = null;

        return $article;
    }


}