<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 13/03/2018
 * Time: 15:54
 */

require 'Client.php';

/**
 * Pour plus d'info sur les repository, lire Domain Driven Design
 * Class ClientRepository
 */
class ClientRepository
{

    /**
     * Retourne tous les clients.
     * @return array d'objet de type Client. Si aucun client n'est trouvé, retourne un tableau vide.
     */
    function findAllClients()
    {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine_commerce', 'root', 'root');

        $result = $dbh->query('SELECT * from client');

        $clients = [];

        foreach($result as $row) {
            // $client = new Client();
            // $client->setId($row['id']);
            // $client->setName($row['name']);
            // $clients[]= $client;

            $clients[]= new Client($row['id'], $row['name']);
        }

        $dbh = null;
        return $clients;
    }

    /**
     * Retourne les 3 derniers clients.
     * @return array d'objet de type Client. Si aucun client n'est trouvé, retourne un tableau vide.
     */
    function findLatest3Clients()
    {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine_commerce', 'root', 'root');

        $result = $dbh->query('SELECT * from client ORDER BY id DESC LIMIT 3');

        $clients = [];

        foreach($result as $row) {
            // $client = new Client();
            // $client->setId($row['id']);
            // $client->setName($row['name']);
            // $clients[]= $client;

            $clients[]= new Client($row['id'], $row['name']);
        }

        $dbh = null;
        return $clients;
    }

    /**
     * Retourne le client dont l'identifiant est précisé en paramètre.
     * @param $id identifiant du client à retourner
     * @return Client si l'identifiant existe. Retourne null sinon.
     */
    public function findById($id)
    {
        $dbh = new PDO('mysql:host=localhost;dbname=lapiscine_commerce', 'root', 'root');

        $stmt = $dbh->prepare('SELECT * from client where id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $result = $stmt->fetch();

        if($result === false) {
            return null;
        }

        $client = new Client($result['id'], $result['name']);

        $dbh = null;

        return $client;
    }


}