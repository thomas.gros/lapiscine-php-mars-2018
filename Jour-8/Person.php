<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 14/03/2018
 * Time: 11:46
 */

class Person
{
    private $age;

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }
}