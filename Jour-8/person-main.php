<?php

require 'Person.php';

$p = new Person();

$p->setAge(12);
echo $p->getAge();

// ...

$p->setAge(18);
echo $p->getAge();