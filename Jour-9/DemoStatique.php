<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 15/03/2018
 * Time: 16:15
 */

namespace demo;


class DemoStatique
{
    public function doSomething()
    {
        echo 'working...';
    }

    public static function doSomethingStatic()
    {
        echo 'working... but static';
    }
}