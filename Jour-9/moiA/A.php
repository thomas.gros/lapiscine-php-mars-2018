<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 15/03/2018
 * Time: 10:10
 */

namespace moiA;

use moiB\B;

class A
{
    public function doSomethingA()
    {
        echo 'A';
        $b = new B();
        $b->doSomethingB();
    }
}