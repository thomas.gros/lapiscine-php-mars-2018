<?php

require 'vendor/autoload.php';
use Ramsey\Uuid\Uuid;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('name');
$log->pushHandler(new StreamHandler(__DIR__.DIRECTORY_SEPARATOR.'demo.log', Logger::WARNING));
$log->info($_SERVER['REQUEST_URI']);

$result = Uuid::uuid4();
$log->error($result->toString());

$result = Uuid::uuid4();
$log->critical($result->toString());