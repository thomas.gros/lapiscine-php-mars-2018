<?php
use moiA\A; //création d'un alias A équivalent à moi\A
use moiC\C; //création d'un alias C équivalent à moi\C

spl_autoload_register(function ($class_name) {
    // 1) extraire le namespace et le nom de la classe
    $namespace = '';
    $class = $class_name;

    $last_antislash_pos = strrpos($class_name, '\\');

    if($last_antislash_pos !== false) {
        $namespace = substr($class_name, 0, $last_antislash_pos);
        $class = substr($class_name, $last_antislash_pos + 1);
    }

    // 2) charger la classe SI elle est à nous
    if(in_array($namespace, ['moiA', 'moiB', 'moiC'])) {
        var_dump($class_name);
        require __DIR__.DIRECTORY_SEPARATOR.$namespace.DIRECTORY_SEPARATOR.$class.".php";
    }

});

$a = new A();
$a->doSomethingA();

$c = new C();