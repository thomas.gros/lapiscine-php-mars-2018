<?php

require 'Human.php';

$h = new Human();

$h->run(10); // le fait de courir nous fatigue. La valeur de fatigue devrait avoir augmenté
var_dump($h->getFatigue());

$h->sleep(10); // le fait de dormir fait baisser la fatigue. La valeur de fatigue devrait avoir diminué
var_dump($h->getFatigue());

$h->sleep(20); // le fait de dormir fait baisser la fatigue. La valeur de fatigue devrait avoir diminué
var_dump($h->getFatigue());

var_dump($h->isAlive()); // au dela de 500 de fatigue, false. En dessous retourner true;


$h->castSpell(); // un sort coute 10 de mana et 100 de fatigue. A la construction on a 100 de mana.
var_dump($h->getMana());
var_dump($h->isAlive());
