<?php

/**
 * Créer une classe représentant un rectangle.
 * Un rectangle a une longueur et une largeur
 * Une rectangle a une méthode qui permet de calculer sa surface
 */

class Rectangle
{
    public $width; // exprimé en mètre
    public $height;

    public function area()
    {
        return $this->width * $this->height;
    }
}