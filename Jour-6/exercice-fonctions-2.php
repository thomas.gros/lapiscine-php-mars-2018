<?php

/**
 * Ecrire
 *
 * - écrire une fonction qui trie un tableau de nombres. Cad: elle prend en paramètre un tableau
 * puis elle retourne un nouveau tableau contenant les nombres dans l'ordre croissant
 */

/**
 * Prend en paramètre un tableau de nombre et vérifie si
 * le nombre 10 est présent dans le tableau. Retourner true/false
 * @param $t
 */
function checkIf10IsPresent($t) {
    foreach ($t as $e) {
        if($e == 10) {
            return true;
        }
    }

    return false;
}



// $tab = [5, 6, 7, 10, 24, 32, 10, 45];
$tab = [5, 6, 7];
$isPresent = checkIf10IsPresent($tab);
var_dump($isPresent);

//check_if_10_is_present($t);
//check_if_ten_is_present($t);

/**
 * une fonction qui retourne le plus petit nombre d'un tableau de nombre passé en paramètre
 */
function findSmallestElement($tab) {

    $smallest = $tab[0];

    foreach ($tab as $e) {
        if($e < $smallest) {
            $smallest = $e;
        }
    }

    return $smallest;
}


$tab = [32, 56, 18];
$smallest = findSmallestElement($tab);
var_dump($smallest);

/**
 * une fonction qui retourne le plus grand nombre d'un tableau de nombre passé en paramètre
 */
