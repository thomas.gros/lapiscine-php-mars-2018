<?php
/**
 * OOP = Object Oriented Programming
 * POO = Programmation Orientée Objet
 */

class Person
{

    // données (variables d'état)
    public $firstname;
    public $lastname;

    // traitements (fonctions methods)
    public function sayHello()
    {
        echo 'hello';
    }

    public function sayWhoIAm()
    {
        echo "$this->firstname $this->lastname";
    }

}

$p1 = new Person();
$p2 = new Person();

var_dump($p1 === $p2);
var_dump($p1);
var_dump($p2);


$p1->firstname = 'Thomas';
var_dump($p1->firstname);
var_dump($p1->lastname);
$p1->sayWhoIAm();


$p2->firstname = 'Bob';
var_dump($p2->firstname);
var_dump($p2->lastname);
$p2->sayWhoIAm();

$p1->sayHello();


