<?php

require 'Product.php';

$livre1 = new Product("un titre", "une description", 12);
var_dump($livre1);

$livre1->setTitle('Les quatre accords toltèques : La voie de la liberté personnelle');
$livre1->setPrice(7.90);
$livre1->setDescription("Il y a des milliers d'années, à travers tout le Sud du Mexique, les Toltèques étaient connus comme des femmes et hommes de connaissance. Les anthropologues les ont décrits comme une nation ou une race, mais en réalité c'était des scientifiques et des artistes formant une société vouée à explorer et préserver la connaissance spirituelle et les pratiques des anciens. Maîtres (naguals) et étudiants se réunissaient à Teotihuacan, l'ancienne cité des pyramides située au-delà de Mexico City, connue comme le lieu où l'Homme devient Dieu.
Au fil des millénaires, les naguals ont été contraints de dissimuler la sagesse ancestrale et de la préserver dans l'ombre. --Ce texte fait référence à une édition épuisée ou non disponible de ce titre.");

var_dump($livre1);