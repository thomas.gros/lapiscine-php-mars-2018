<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 12/03/2018
 * Time: 14:13
 */

class Human
{
    const INITIAL_FATIGUE = 0;
    const INITIAL_MANA = 100;
    const MAX_FATIGUE = 500;

    private $isAlive;
    private $fatigue;
    private $mana;

    /**
     * Human constructor.
     */
    public function __construct()
    {
        $this->isAlive = true;
        $this->fatigue = INITIAL_FATIGUE;
        $this->mana = INITIAL_MANA;
    }

    /**
     * @return mixed
     */
    public function getFatigue()
    {
        return $this->fatigue;
    }

    /**
     * @return int
     */
    public function getMana()
    {
        return $this->mana;
    }

    public function isAlive()
    {
        return $this->isAlive;
    }

    public function run($duration)
    {
        if(!$this->isAlive) { // TODO erreur
            return;
        }

        $this->fatigue = $this->fatigue + 10 * $duration;

        if($this->fatigue > MAX_FATIGUE) {
            $this->isAlive = false;
        }
    }

    public function sleep($duration)
    {
        if(!$this->isAlive) { // TODO erreur
            return;
        }

        $this->fatigue = max(0, $this->fatigue - 5 * $duration);
    }


    public function castSpell()
    {
        if(!$this->isAlive) { // TODO erreur
            return;
        }

        if($this->mana < 10) { // impossible de jetter un sort
            return;
        }

        $this->mana = max(0, $this->mana - 10);
        $this->fatigue = $this->fatigue + 100;

        if($this->fatigue > MAX_FATIGUE) {
            $this->isAlive = false;
        }
    }

}