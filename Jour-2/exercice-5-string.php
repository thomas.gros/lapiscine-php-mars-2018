<?php

$s = $_GET['str'];

/**
 * 1) Compter le nombre de 'o' dans $s
 * 2) Compter le nombre de voyelles dans la $s
 */

$nb_de_o = 0;
for($i = 0; $i < strlen($s); $i++) {
    $current_char = substr($s, $i, 1);

    if($current_char == 'o') {
        $nb_de_o++;
    }
}

echo $nb_de_o;
