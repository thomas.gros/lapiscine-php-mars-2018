<?php

for ($i = 0; $i < 10 ; $i++) {
    echo $i;
}

/**
 * i est pair si i % 2 == 0
 * Si i est pair afficher "0 : pair"
 * Si i est impair afficher "5: impair"
 */
for ($i = 0; $i < 10 ; $i++) {
    if ($i % 2 == 0) {
        // echo $i . " est pair";
        echo "$i est pair";
    } else {
        echo "$i est impair";
    }

}

/**
 * Pour tous les nombres de 0 à 100
 * Si le nombre est un multiple de 3 alors afficher la valeur du nombre + 'FIZZ'
 * Si le nombre est un multiple de 5 alors afficher la valeur du nombre + 'BUZZ'
 * Si le nombre est un multiple de 3 et de 5 alors afficher la valeur du nombre + 'FIZZBUZZ'
 */
for ($i = 0; $i < 100; $i++) {
    if (($i % 3 == 0) && ($i % 5 == 0)) {
        echo "$i FIZZBUZZ";
    } elseif ( $i % 3 == 0) {
        echo "$i FIZZ";
    } elseif ( $i % 5 == 0) {
        echo "$i BUZZ";
    } else {

    }
}















