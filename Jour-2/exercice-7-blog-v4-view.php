<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="exercice-7.css">
</head>
<body>

<section class="articles">

<?php foreach($articles as $article) { ?>
    <article class="article">
       <header>
           <h3><?php echo $article['title'] ?></h3>
           <p><?php echo $article['date'] ?></p>
       </header>
        <div class="content"><?php echo $article['content'] ?></div>
    </article>
<?php } ?>

</section>

<section class="articles">

    <?php foreach($articles as $article) : ?>
        <article class="article">
            <header>
                <h3><?= $article['title'] ?></h3>
                <p><?= $article['date'] ?></p>
            </header>
            <div class="content"><?= $article['content'] ?></div>
        </article>
    <?php endforeach ?>

</section>

</body>
</html>