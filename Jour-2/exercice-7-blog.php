<?php



echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Title</title>
    <style>
        header {
            display: flex;
            background-color:green;
            align-items: center;
        }

        header h3 {
            flex: 1;
            background-color: red;
            font-size:24px;
        }

        header p {
            background-color: blue;
        }
    </style>
</head>
<body>";

echo '<section class="articles">';

foreach($articles as $article) {

    echo "
    <article class=\"article\">
       <header>
           <h3>".$article['title']."</h3>
           <p>".$article['date']."</p>
       </header>
        <div class=\"content\">".$article['content']."</div>
    </article>";
}
echo '</section>';

echo "</body>
</html>";