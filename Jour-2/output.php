<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        header {
            display: flex;
            background-color:green;
            align-items: center;
        }

        header h3 {
            flex: 1;
            background-color: red;
            font-size:24px;
        }

        header p {
            background-color: blue;
        }
    </style>
</head>
<body>
<?php echo '<h1>du contenu</h1>'; ?>
</body>
</html>