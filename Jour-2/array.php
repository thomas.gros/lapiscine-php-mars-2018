<?php

$arr = [1,2,3,4,5];
$arr_2 = ['bonjour', 'le', 'monde'];
$arr_3 = [1,'bonjour', 2, 'le', 3, 'monde'];


var_dump($arr);
var_dump($arr_2);

var_dump($arr[0]);
var_dump($arr_2[0]);


$arr_4 = [
    'FFFFFF' => 'white',
    '000000' => 'black',
    'FF0000' => 'red',
    '00FF00' => 'green',
    '0000FF' => 'blue'
];

var_dump($arr_4['00FF00']);
var_dump($arr_4[3]);

// parcourir un tableau indicé numériquement
for($i = 0; $i < count($arr); $i++) {
    echo $arr[$i];
}

for($i = 0; $i < count($arr_2); $i++) {
    echo $arr_2[$i];
}

// parcourir un tableau séquentiellement, quelque soit le tableau
foreach($arr as $v) {
    echo $v;
}

// [1,2,3,4,5]
foreach($arr as $k => $v) {
    echo "$k : $v";
}

// [’bonjour', 'le', 'monde']
foreach($arr_2 as $k => $v) {
    echo "$k : $v";
}

foreach($arr_4 as $k => $v) {
    echo "$k : $v";
}






