<?php
$arr = [[1,2,3], [4,5,6], [7,8,9]];

$a1 = $arr[0]; // [1,2,3]
$a2 = $arr[1]; // [4,5,6]

$a1[0]; // 1
$a1[1]; // 2

$arr[0][2]; // 3

$personnes = [["bill", "gates"], ["steve", "jobs"]];

$personnes[0][1];

$personnes_2 = [
    ["firstname" => "bill", "lastname" => "gates"],
    ["firstname" => "steve", "lastname" => "jobs"]
];

$personnes_2[0]["lastname"];