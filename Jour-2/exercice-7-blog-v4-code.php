<?php

$articles = [
    [
        0 => "PHP 7.1.15 Released",
        "title" => "PHP 7.1.15 Released",
        1 => "02 Mar 2018",
        "date" => "02 Mar 2018",
        2 => "The PHP development team announces the immediate availability of PHP 7.1.15. This is a security fix release, containing one security fix and many bug fixes. All PHP 7.1 users are encouraged to upgrade to this version. For source downloads of PHP 7.1.15 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog.",
        "content"=> "The PHP development team announces the immediate availability of PHP 7.1.15. This is a security fix release, containing one security fix and many bug fixes. All PHP 7.1 users are encouraged to upgrade to this version. For source downloads of PHP 7.1.15 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog."
    ],
    [
        "title" => "PHP 5.6.34 Released",
        "date" => "01 Mar 2018",
        "content"=> "The PHP development team announces the immediate availability of PHP 5.6.34. This is a security release. One security bug was fixed in this release. All PHP 5.6 users are encouraged to upgrade to this version. For source downloads of PHP 5.6.34 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog."
    ],
    [
        "title" => "PHP 7.2.3 Released",
        "date" => "01 Mar 2018",
        "content"=> "The PHP development team announces the immediate availability of PHP 7.2.3. This is a security release with also contains several minor bug fixes. All PHP 7.2 users are encouraged to upgrade to this version. For source downloads of PHP 7.2.3 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog."
    ],
];

include 'exercice-7-blog-v4-view.php';