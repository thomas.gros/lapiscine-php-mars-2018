<?php

/**
 * /LaPiscine/exercice-3.php?format=xml
 * /LaPiscine/exercice-3.php?format=html
 * /LaPiscine/exercice-3.php?format=text
 * /LaPiscine/exercice-3.php?format=json
 *
 * Retourner le nombre 42
 *
 * Au format xml <nombre>42</nombre>
 * Au format html <p>42</p>
 * Au format texte 42
 * Au format json {"nombre":42}
 *
 *
 * Dans une réponse HTTP, il y a un header qui doit indiquer le type du contenu
 *
 * echo => écrit dans le contenu "body" de la reponse
 *
 */

//if ($_GET['format'] == 'xml') {
//    header('Content-Type:application/xml');
//    echo '<nombre>42</nombre>';
//} elseif ($_GET['format'] == 'html') {
//    header('Content-Type:text/html');
//    echo '<p>42</p>';
//} elseif ($_GET['format'] == 'text') {
//    header('Content-Type:text/plain');
//    echo 42;
//} elseif ($_GET['format'] == 'json') {
//    header('Content-Type:application/json');
//    echo '{"nombre":42}';
//} else {
//    // TODO format non supporté
//}


switch($_GET['format']) {
    case 'xml':
        header('Content-Type:application/xml');
        echo '<nombre>42</nombre>';
        break;
    case 'html':
        header('Content-Type:text/html');
        echo '<p>42</p>';
        break;
    case 'text':
        header('Content-Type:text/plain');
        echo 42;
        break;
    case 'json':
        header('Content-Type:application/json');
        echo '{"nombre":42}';
        break;
    default:
        // TODO format non supporté
}

