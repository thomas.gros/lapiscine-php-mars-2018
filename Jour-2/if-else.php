<?php

// if simple

if (5 > 2) {
    echo '5 > 2';
}

if ((5 > 2) && (3 < 4)) {
    echo '5 > 2 && 3 < 4';
}

$res = 5 > 2;

if ($res) { // eviter d'écrire $res  == TRUE
    //...
}


// if .. else

if (5 > 12) {
    echo '...';
} else {
    echo '.....';
}

// if .. elseif ... else

$a = 42;
if ($a < 10) {
   echo 'a < 10';
} elseif ($a < 20) {
    echo '10 <= a < 20';
} elseif ($a < 30) {
    echo '20 <= a < 30';
} else {
    echo 'a >= 30';
}

