<?php

$colors = [
    'FFFFFF' => 'white',
    '000000' => 'black',
    'FF0000' => 'red',
    '00FF00' => 'green',
    '0000FF' => 'blue'
];

echo "<table>";
foreach($colors as $key => $value) {
    echo "<tr><td>$key</td><td>$value</td></tr>";
}
echo "</table>";


