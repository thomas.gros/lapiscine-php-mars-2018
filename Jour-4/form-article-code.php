<?php

// POST = le formulaire a été soumis par l'utilisateur
if("POST" == $_SERVER["REQUEST_METHOD"]) {
        $form_data = [];

        if(array_key_exists('title', $_POST)) {
            $form_data['title'] = $_POST['title'];
        }

        if(array_key_exists('content', $_POST)) {
            $form_data['content'] = $_POST['content'];
        }

        // Vérification de la validité des données envoyées dans la requête POST

        $errors = []; // contient toutes les erreurs du formulaire, champs par champs

        // champ title - doit être plus de 5 caractères, et ne doit pas contenir de underscore
        $title_field = $form_data['title'];
        $errors_title_field = [];
        if(strlen($title_field) < 6) { // plus de 6 caractères
            $errors_title_field[] = "le titre doit être de plus de 5 caractères";
        }
        if(strpos($title_field, '_') !== FALSE) { // pas de underscore
            $errors_title_field[] = "le titre ne doit pas contenir de underscore";
        }

        if(count($errors_title_field) > 0) {
            $errors["title"] = $errors_title_field;
        }

        // champ content doit être de 10 caractères minimum
        $content_field = $form_data['content'];
        $errors_content_field = [];
        if(strlen($content_field) < 10) { // minimum 10 caractères
            $errors_content_field[]= "le contenu doit au minimum de 10 caractères";
        }

        if(count($errors_content_field) > 0) {
            $errors["content"] = $errors_content_field;
        }

        // Suite des traitements après validation
        if(count($errors) == 0) { // aucune erreur de validation
            // faire le traitement normal

            $dbh = new PDO('mysql:host=localhost;dbname=lapiscine', 'root', 'root');

            $stmt = $dbh->prepare('INSERT INTO article (title, content) VALUES (:title, :content)');
            $stmt->bindParam(':title', $form_data['title']);
            $stmt->bindParam(':content', $form_data['content']);
            $stmt->execute();

            $dbh = null;

            // Quand il n'y a pas d'erreur => faire une redirection HTTP
            header('Location:http://localhost:8888/LaPiscine/Jour-3/master/master-code.php');
            exit(); // sortir car sinon on raffiche le formulaire
        }
}

// afficher le formulaire si GET ou si erreurs détectées
include 'form-article-view.php';