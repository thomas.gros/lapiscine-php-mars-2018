<?php
session_start();

// *) vérifier que l'utilisateur existe

$dbh = new PDO('mysql:host=localhost;dbname=lapiscine', 'root', 'root');

$stmt = $dbh->prepare('SELECT count(*) FROM user WHERE username = :username AND password = :password');
$stmt->bindParam(':username', $_POST['username']);
$stmt->bindParam(':password', $_POST['password']); // dans la vraie vie comparer avec le password crypté...
$stmt->execute();

$results = $stmt->fetchAll();

if($results[0][0] == 1) {
    $_SESSION['username'] = $_POST['username'];
    header('Location:http://localhost:8888/LaPiscine/Jour-4/profil-code.php');
    exit();
}

require 'login-form-view.php';