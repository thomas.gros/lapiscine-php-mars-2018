<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<form action="" method="post">
    <div>
        <label for="title">titre</label>
        <input id="title" name="title" type="text" <?php if(isset($form_data) && array_key_exists('title', $form_data)) { echo "value=\"$form_data[title]\""; } ?> >
        <span>
        <?php
            if(isset($errors) && array_key_exists("title", $errors)) {
                foreach($errors["title"] as $err) {
                    echo "$err. ";
                }
            }
        ?>
        </span>
    </div>

    <div>
        <label for="content">contenu</label>
        <textarea id="content" name="content" cols="30" rows="10"><?php if(isset($form_data) && array_key_exists('content', $form_data)) { echo $form_data['content']; } ?></textarea>
        <span>
        <?php
            if(isset($errors) && array_key_exists("content", $errors)) {
                foreach($errors["content"] as $err) {
                    echo "$err. ";
                }
            }
        ?>
        </span>
    </div>

    <div>
        <input type="submit" value="créer un article">
    </div>
</form>

</body>
</html>