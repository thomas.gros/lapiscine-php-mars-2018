<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 12/03/2018
 * Time: 17:07
 */

class Article
{

    private $id;
    private $title;
    private $description;
    private $price;

    /**
     * Article constructor.
     * @param $id
     * @param $title
     * @param $description
     * @param $price
     */
    public function __construct($id, $title, $description, $price)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }


}