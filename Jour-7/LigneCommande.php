<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 13/03/2018
 * Time: 09:52
 */

class LigneCommande
{

    private $article;
    private $qty;

    /**
     * LigneCommande constructor.
     * @param $article
     * @param $qty
     */
    public function __construct($article, $qty)
    {
        $this->article = $article;
        $this->qty = $qty;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }
}