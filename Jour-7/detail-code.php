<?php

require 'ArticleRepository.php';

$id = $_GET['id'];

$articleRepository = new ArticleRepository();
$article = $articleRepository->findById($id);

if($article === null) {
    http_response_code(404);
    echo 'page not found';
} else {
    include 'detail-view.php';
}