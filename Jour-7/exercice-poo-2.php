<?php

/**
 * 1) Créer les tables article, client, commande, ligne_commande.
 * De manière à pouvoir persister les classes Article, Client, Commande, LigneCommande
 *
 * 2) Créer un jeu de test: 5 articles, 3 clients, 3 commandes (avec lignes de commandes)
 *
 * 3) Créer une classe ArticleRepository. Cette classe va permettre d'accéder aux
 * articles présents dans la base de données
 *
 * 4) Créer une méthode findAllArticles dans ArticleRepository. Cette méthode retourne
 * un tableau contenant des objets Articles correspondant à tous les articles présents en
 * base de données.
 *
 *
 */

require 'ArticleRepository.php';
$articleRepository = new ArticleRepository();

//$articles = $articleRepository->findAllArticles();
//var_dump($articles);

$article = $articleRepository->findById(1);
var_dump($article);