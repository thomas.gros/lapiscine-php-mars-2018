-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 13, 2018 at 02:19 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `description`, `price`) VALUES
(1, 'PHP 7: Cours et exercices', 'Présentation de l\'éditeur\r\nCe manuel d\'initiation vous conduira des premiers pas en PHP jusqu\'à la réalisation d\'un site Web dynamique complet interagissant avec une base de données MySQL ou SQLite . Après avoir appris à installer PHP et à créer quelques pages simples, vous étudierez en détail la syntaxe du langage (variables, types de données, opérateurs, instructions, fonctions, tableaux...), avant de progresser rapidement vers des sujets de niveau plus avancé : programmation objet, manipulation des chaînes de caractères et expressions régulières, gestion de mails, sessions et cookies, accès aux bases de données MySQL et SQLite, traitements XML, etc.  L\'ouvrage met l\'accent sur les nouveautés de PHP 7.\r\nBiographie de l\'auteur\r\nEnseignant en mathématiques et consultant web, Jean Engels est auteur de nombreux ouvrages portant sur les technologies du Web : HTML, CSS, JavaScript, PHP et MySQL.', '29.90'),
(2, 'Concevez votre site web avec PHP et MySQL', 'Description du produit\r\nRevue de presse\r\nL\'Informaticien recommande ce livre\r\nVous n\'y connaissez rien et vous avez le désir de créer votre site web. Le site du Zéro vient de publier l\'ouvrage qu\'il vous faut pour y parvenir. Le livre rappelle les bases du PHP et vous donne les conseils qui vous aiderons à arriver au bout de votre projet.\r\n\r\n--L\'Informaticien du 4 juin 2010 --Ce texte fait référence à une édition épuisée ou non disponible de ce titre.\r\nPrésentation de l\'éditeur\r\nBlogs, réseaux sociaux, pages d\'accueil personnalisables... Depuis quelques années, les sites web ont gagné en fonctionnalités et sont devenus dans le même temps de plus en plus complexes. Que le temps de la \"page web perso\" est loin ! Aujourd\'hui, il faut que ça bouge ! On veut voir des actualités sur la page d\'accueil, on veut pouvoir les commenter, discuter sur des forums, bref, participer à la vie du site. Le langage PHP a justement été conçu pour créer des sites \"vivants\" (on parle de sites dynamiques). Si vous voulez apprendre à créer vous aussi des sites web dynamiques et que vous êtes débutant en programmation, ce livre est pour vous !', '29.00'),
(3, 'Réalisez votre site web avec HTML 5 et CSS 3', 'Revue de presse\r\nLe pied à l\'étrier. \r\nPour qui souhaite se lancer dans le développement d\'un site web en exploitant le HTML5 et le CSS3, ce manuel, moins ardu que le précédent, apportera des conseils précieux. Il est issu des cours dispensés sur le Site du Zéro et, tout comme ses prédécesseurs dans la collection - une dizaine déjà - séduit par une présentation claire et aérée. L\'auteur y introduit le HTML5 ainsi que la mise en forme avec le CSS avant d\'aborder la mise en page du site puis les fonctionnalités un peu plus ardues comme les tableaux, les formulaires ou la gestion des vidéos. Une bonne synthèse de la boîte à outils indispensable. A conseiller à tous les débutants pour la création d\'un site statique mais au goût du jour ! --L\'Informaticien n° 99 / Février 2012 --Ce texte fait référence à une édition épuisée ou non disponible de ce titre.\r\nPrésentation de l\'éditeur\r\nVous rêvez d\'apprendre à créer des sites web mais vous avez peur que ce soit compliqué car vous débutez ? Ce livre est fait pour vous ! Conçu pour les débutants, il vous permettra de découvrir HTML5 et CSS3, les dernières technologies en matière de création de sites web, de façon progressive et sans aucun pré-requis, à part savoir allumer son ordinateur ! Qu\'allez-vous apprendre ? Qu\'est-ce qu\'un site web et comment font les pros pour en créer ? Installez les logiciels dont vous avez besoin, que vous soyez sous Windows, Mac ou Linux Découvrez les balises HTML5 afin de structurer votre site Habillez vos pages web grâce à CSS3 Réussissez la mise en page de votre site pas à pas à travers des travaux pratiques ! Insérez facilement des images et vidéos sur votre site web Hébergez votre site sur le Web', '26.90'),
(4, 'JavaScript pour les Nuls grand format, 2e édition', 'Présentation de l\'éditeur\r\nContrairement à la chanson, \'Quand le jazz est, quand le jazz est là\', le Java reste. Et il s\'écrit aussi en scripts !\r\n\r\nNon, JavaScript ce n\'est pas le nom de la dernière danse à la mode ! C\'est un langage de programmation qui permet d\'animer les pages Web de manière simple et efficace, et ce n\'est pas parce qu\'on dit langage de programmation, que c\'est obligatoirement réservé à une élite. JavaScript pour les Nuls est l\'outil indispensable pour bien débuter.\r\n\r\nAu programme :\r\n\r\n\r\n\r\nÉcrivez votre tout premier script\r\nLes concepts de la programmation JavaScript\r\nEspionnage : détection du navigateur utilisé par vos visiteurs\r\nLa bonne cuisine des cookies\r\nImages réactives et interactives\r\nLes rollovers\r\nExamen des saisies de l\'utilisateur\r\nDynamisez vos pages\r\n\r\nBiographie de l\'auteur\r\nChris Minnick et Eva Holland sont auteurs de nombreux livres sur la programmation et le développement Web, ils sont également conférenciers et enseignants en université.', '24.95'),
(5, 'SQL - Les fondamentaux du langage (3e édition)', 'Présentation de l\'éditeur\r\nCe livre sur les fondamentaux du langage SQL s\'adresse aux développeurs et informaticiens débutants appelés à travailler avec un Système de Gestion de Bases de Données Relationnelles (SGBDR) pour stocker et manipuler des données. Son objectif est de décrire les ordres principaux les plus utilisés du langage SQL (indépendamment des déclinaisons réalisées par les éditeurs de SGBDR) pour permettre au lecteur de prendre en main rapidement une base de données relationnelle et être capable de créer des tables, de les interroger, de les modifier, d\'insérer et de supprimer des lignes. Le livre débute par un bref historique sur la création de la norme SQL puis présente quelques notions sur le modèle relationnel. Ensuite, chaque chapitre présente une subdivision de SQL ; la création et la manipulation des tables puis la gestion des données dans ces tables en incluant les dernières évolutions comme les fonctions de fenêtrage. L\'auteur enchaîne avec la sécurité des données et quelques notions de transactions, puis présente la programmation avec quelques notions de PL/SQL et l\'étude des déclencheurs. Le livre se termine en abordant des thèmes un peu plus complexes comme les chargements en masse, les imports et exports de tables, les notions de performances ou encore les objets systèmes. Les exemples utilisés dans ce livre ont été réalisés avec la version Oracle 12c (12.1.0.2) DB Developer VM, SQL Server 2016 SP1 Developer Edition, la version MySQL 5.7.11, PostgreSQL en version 9.5.6 et sont en téléchargement sur le site www.editions-eni.fr.\r\nBiographie de l\'auteur\r\nAnne-Christine BISSON est consultante indépendante en informatique décisionnelle. Cette experte conseille sur la conception de bases et entrepôts de données de différents SGDB. à ce titre, elle manipule et agrège des données à partir de sources diverses pour les restituer de façon synthétique dans des entreprises et administrations de différents secteurs. Egalement formatrice, elle prolonge avec ce livre sa volonté de partager ses connaissances sur SQL auprès des lecteurs. Eric GODOC est Directeur de projets informatiques dans une SSII. Ses projets autour du développement et des migrations d\'applications au sein de grandes entreprises lui ont apporté une expérience significative sur la manipulation de bases de données relationnelles. Avec ce livre il fait bien sûr profiter les lecteurs de ce retour d\'expérience et surtout, il leur fournit les moyens de maîtriser les bases du langage SQL. Son objectif est qu\'ils puissent répondre à la majorité des besoins dans l\'utilisation d\'une base de données relationnelle, quel qu\'en soit l\'éditeur.', '29.90');

-- --------------------------------------------------------

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`) VALUES
(1, 'steve'),
(2, 'bill'),
(3, 'bob');

-- --------------------------------------------------------

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`id`, `date`, `client_id`) VALUES
(1, '2018-03-13 00:00:00', 1),
(2, '2018-03-06 00:00:00', 2),
(3, '2018-03-11 00:00:00', 3);

-- --------------------------------------------------------

--
-- Dumping data for table `ligne_commande`
--

INSERT INTO `ligne_commande` (`id`, `article_id`, `commande_id`, `qty`) VALUES
(1, 2, 1, 1),
(2, 4, 1, 3),
(3, 1, 2, 10),
(4, 3, 2, 3),
(5, 5, 3, 5),
(6, 2, 3, 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
