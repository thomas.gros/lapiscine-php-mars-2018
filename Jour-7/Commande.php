<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 12/03/2018
 * Time: 17:14
 */

require 'LigneCommande.php';

class Commande
{

    private $id;
    private $client;
    private $date;
    private $lignesCommande;

    /**
     * Commande constructor.
     * @param $id
     * @param $client
     * @param $date
     */
    public function __construct($id, $client, $date)
    {
        $this->id = $id;
        $this->client = $client;
        $this->date = $date;
        $this->lignesCommande = [];
    }

    public function addArticle($article, $qty)
    {
        $lc = new LigneCommande($article, $qty);
        $this->lignesCommande[]= $lc;
    }

    public function computeTotal()
    {
        $total = 0;
        foreach($this->lignesCommande as $lc) {
            $total += $lc->getArticle()->getPrice() * $lc->getQty();
        }

        return $total;
    }


}