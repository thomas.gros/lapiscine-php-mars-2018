<?php
/**
 * Représenter une commande
 *
 * Une commande est passée par un client
 * Une commande est passée à une certaine date
 *
 * Une commande peut contenir plusieurs produits
 * Dans une commande un produit peut être commandé dans une certaine quantité
 *
 *
 * Un article: id, titre, description, prix
 * Un client: id, nom
 *
 * Representer ces notions d'article, client, commande sous la forme d'objets
 */

require 'Article.php';
require 'Client.php';
require 'Commande.php';

$article1 = new Article(1, "voiture", "une super voiture", 10000);
$article2 = new Article(2, "avion", "un très bel avion", 500000);

$client = new Client(1, "Steve");

$commande = new Commande(1, $client, "2017-10-10");
$commande->addArticle($article1, 3);
$commande->addArticle($article2, 10);

echo "Total de la commande = " . $commande->computeTotal();
