<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 12/03/2018
 * Time: 17:12
 */

class Client
{

    private $id;
    private $name;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}