<?php

// booleans

echo TRUE;
echo FALSE;

echo TRUE == 1;
echo FALSE == 0;

// integers

echo 0;
echo 1;
echo -2;
echo 1 + 1;
echo 3 - 4;
echo 5 * 10;
echo 10 / 5;

echo 3 / 2;

echo 112;   //  100 + 12   = 1 * 10^2 + 1 * 10^1 + 2 * 10^0
echo 0x4316;   // <=> 4 * 16^3  + 3 * 16^2 + 1 * 16^1  6 * 16 ^ 0
echo 0b11001;  // <=> 1 * 2^4 + 1 * 2^3 + 0 * 2^2 + 0 * 2^1 + 1 * 2^0
echo 0123;  // <=> 1 * 8^3 + 2 * 8^1 + 3 * 8^0

// background-color:
// #00FF00 (RGB)   = rgb(0, 255, 0)
//   R = 0x00
//   G= 0xFF =  F * 16^1 + F * 16^0  = F * 16 + F * 1 = 15 * 16 + 15 * 1 = 240 + 15 = 255
//   B = 0x00

// #A3B2C6
//  R = 0xA3 <=> A * 16^1 + 3 * 16^0 = 10 * 16 + 3 * 1 = 163
//  G = 0xB2 <=> B * 16^1 + 2 * 16^0 = 11 * 16 + 2 * 1 = 178
//  B = 0xC6 <=> C * 16^1 + 6 * 16^0 = 12 * 16 + 6 * 1 = 198

// 0b110011 = 1 * 2^5 +  1* 2^4 + 1 * 2^1 + 1 * 2^0 = 32 + 16 + 2 + 1 = 51
// 0b101    = 1 * 2^2 + 0 * 2^1 + 1 * 2^0 = 4 + 1 = 5

// 0543     = 5 * 8^2 + 4 * 8^1 + 3 * 8^0 = 5 * 64 + 32 + 3 = 320 + 32 + 3 = 355

echo 255;
echo 0xFF;

// a % b = reste de la division entière de a par b.     a = b * q + r
// 5 % 2  =>   5 = 2 * q + r   q = 2   r = modulo = 1

echo 10 % 2; // 0 modulo
echo 5 % 2; // 1
echo 6 % 2; // 0
echo 7 % 2; // 1
echo 10 % 3;

// floats
// 1 / 3 = 0,333333333333333333333333333
echo 1.2;

// strings

echo 'hello';
echo "hello entre double quotes";

echo 'bonjour '.'le monde'; // concaténation


// conversions implicites
echo '1' + 1; // 2
echo 'a' + 1; // 1
echo 'a5' + 1; // 1
echo '5a' + 1; // 6
