<?php

// création de variable
$thisIsAVariable = 42;
echo $thisIsAVariable;
var_dump($thisIsAVariable);

$thisIsAVariable = 'toto';
echo $thisIsAVariable;


// affichage du contenu d'une variable
var_dump($thisIsAVariable);

var_dump(TRUE);
var_dump(FALSE);

// variables prédéfinies
var_dump($_GET);
var_dump($_SERVER);
