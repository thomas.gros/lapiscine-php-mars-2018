<?php
/**
 * Ecrire un script PHP qui génère une réponse HTTP de type html
 * contenant une table qui récapitule certaines informations de la requête HTTP
 * - host
 * - uri
 * - user-agent

 * Hint: regarder les clés de la variable $_SERVER, par exemple $_SERVER['HTTP_USER_AGENT']
 */

// volontairement ignoré la structure 'correcte' d'un doc HTML: doctype, html, head, body,...

echo '<table border="1">
        <thead>
            <tr>
            <th>Entête</th><th>Valeur</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>host</td><td>'. $_SERVER['HTTP_HOST'] .'</td>
        </tr>
        <tr>
            <td>uri</td><td>' . $_SERVER['REQUEST_URI'] . '</td>
        </tr>
        <tr>
            <td>User-agent</td><td>' . $_SERVER['HTTP_USER_AGENT'] . '</td>
        </tr>
    </tbody>
</table>';