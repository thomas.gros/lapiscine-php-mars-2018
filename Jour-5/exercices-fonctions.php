<?php

/**
 *  Une fonction qui prend en paramètre un tableau et affiche (echo) tous les éléments du tableau
 */
function affiche($tableau) {
    foreach ($tableau as $e) {
        echo $e;
    }
}

$t = [1,2,3,4,5];
affiche($t); // $tableau = $t
affiche([1,2,3,4,5]); // $tableau = [1,2,3,4,5]

/*
 * une fonction qui calcule la moyenne des nombres d'un tableau (passé en paramètre). La fonction
 * retourne la moyenne.
 */
function moyenne($tab) {
    return array_sum($tab) / count($tab);
}

function moyenne_v2($tab) {
    $sum = 0;

    foreach ($tab as $e) {
        $sum = $sum + $e; // $sum += $e
    }

    return $sum / count($tab);
}

$t = [1,2,3,4,5];
echo moyenne($t);
echo moyenne_v2($t);

/**
 * Une fonction qui prend en paramètre un tableau de nombres
 * et qui retourne un tableau contenant les éléments supérieurs à 15.
 */
function plusGrandQueQuinze($input) {
    $result = []; // $result contient un tableau vide

    foreach($input as $e) {
        if($e > 15) {
            $result[]= $e;
        }
    }

    return $result;
}


$t_input = [1,2,50, 32, 12, 18];
$t_output = plusGrandQueQuinze($t_input);
var_dump($t_output);
