<?php

// ecrire une fonction qui permet de faire fizzbuzz
// de 0 jusqu'a un nombre paramétrable
// ex:
// fizzbuzz(100);
// fizzbuzz(30);
// fizzbuzz(10);
// fizzbuzz(500);

function fizzbuzz($n) {
    for($i = 0; $i < $n; $i++) {
        if ($i % 3 == 0 && $i % 5 == 0) {
           echo "$i: fizzbuzz";
        } else if ($i %3 == 0) {
            echo "$i : fizz";
        } else if($i % 5 == 0) {
            echo "$i : buzz";
        }
    }
}

fizzbuzz(10);
fizzbuzz(50);