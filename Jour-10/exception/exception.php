<?php

function uneFonctionQuiJetteUneException() {
    $monException = new Exception('ceci est la description de l erreur', 12345);
    throw $monException;
}

try {
    uneFonctionQuiJetteUneException();
    echo "pas d'erreur, tout va bien";
} catch(Exception $e) {
    // TODO faire du traitement
    echo 'erreur détectée - veuillez contacter le support';
    echo $e->getMessage();
    echo $e->getCode();
    echo $e->getFile();
    echo $e->getLine();
}

echo 'fin du programme...';