<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 16/03/2018
 * Time: 15:02
 */

use PHPUnit\Framework\TestCase;

class MathTest extends TestCase
{
    public function testCanInverseAnInteger()
    {
        // AAA - Given/When/Then
        // Arrange
        $math = new Math();
        $x = 2;

        // Act
        $result = $math->inverse($x);

        // Assert
        $this->assertEquals(0.5, $result, "l'inverse de 2 est 0.5");
    }

    public function testCannotInvertAString()
    {
        $this->expectException(Exception::class);

        // AAA - Given/When/Then
        // Arrange
        $math = new Math();
        $x = '2';

        // Act
        $math->inverse($x);

        // Assert
        // exception cf plus haut

    }

    public function testCannotInvertZero()
    {
        $this->expectException(Exception::class);

        // AAA - Given/When/Then
        // Arrange
        $math = new Math();
        $x = 0;

        // Act
        $math->inverse($x);

        // Assert
        // exception cf plus haut

    }
}