<?php

class Math
{
    /**
     * Retourne l'inverse d'un nombre entier.
     * Ce nombre doit être différent de 0.
     * @param int le nombre a inverser
     * @return int 1/$x
     * @throws Exception si $x == 0
     */
    function inverse($x) {
        if(! is_int($x) ) {
            //if(gettype($x) !== 'integer') {
            throw new Exception("x doit être un nombre entier");
        }
        if($x === 0) {
            throw new Exception('Division par 0 non autorisée');
        }
        return 1/$x;
    }
}