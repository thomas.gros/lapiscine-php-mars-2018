<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 16/03/2018
 * Time: 10:18
 */

namespace Inheritance;


use symfony\Controller;

class AccueilController extends Controller
{

    public function affichePageAccueil()
    {
        require '../Model/ClientRepository.php';
        $clientRepository = new ClientRepository();
        $clients = $clientRepository->findAllClients();


        $this->render('client-master-view.php', ["clients" => $clients]);
    }
}