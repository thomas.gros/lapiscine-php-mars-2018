<?php

require 'Animal.php';
require 'Cat.php';
require 'Dog.php';

$c = new \Inheritance\Cat();

$c->setAge(12);
var_dump($c->getAge());

$d = new \Inheritance\Dog();

$d->setAge(5);
var_dump($d->getAge());

// $a = new \Inheritance\Animal(); interdit car Animal est abstract