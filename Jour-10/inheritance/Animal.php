<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 16/03/2018
 * Time: 09:51
 */

namespace Inheritance;

abstract class Animal
{

    protected $name; // accessible depuis les classes qui héritent de Animal
    private $age; // accessible uniquement depuis la classe Animal

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

}